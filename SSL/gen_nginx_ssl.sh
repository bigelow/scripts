if [ -z "$1" ]
  then
    echo "Supply domain name"
    exit 1
fi
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
sudo rm /etc/nginx/snippets/ssl-$1.conf
sudo touch /etc/nginx/snippets/ssl-$1.conf
sudo sh -c "echo \"ssl_certificate /etc/letsencrypt/live/$1/fullchain.pem;\" >> /etc/nginx/snippets/ssl-$1.conf"
sudo sh -c "echo \"ssl_certificate_key /etc/letsencrypt/live/$1/privkey.pem;\" >> /etc/nginx/snippets/ssl-$1.conf"
sudo cp ssl-params.conf /etc/nginx/snippets/

cat << EOF
server {
    listen 80;
    listen [::]:80;
    server_name $1 www.$1;
EOF
echo '    return 301 https://$server_name$request_uri;'
cat << EOF
}

server {

    # SSL configuration

    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    include snippets/ssl-$1.conf;
    include snippets/ssl-params.conf;
    server_name $1 www.$1;
    index index.html;
    root $PWD;
}
EOF

