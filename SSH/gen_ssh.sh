#!/bin/bash
{
if [ -f ~/.ssh/id_rsa ]; then
    echo "Key already exists"
    exit 0
fi
}
ssh-keygen -t rsa -b 4096 -C "tectoservers" -N "" -f ~/.ssh/id_rsa
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
cat ~/.ssh/id_rsa.pub
