if [ -z "$1" ]
  then
    echo "Supply domain name"
    exit 1
fi
sudo touch /etc/nginx/sites-available/$1
sudo nano /etc/nginx/sites-available/$1
sudo ln -s /etc/nginx/sites-available/$1 /etc/nginx/sites-enabled/$1
sudo nginx -t
echo "Nginx not restarted yet"
