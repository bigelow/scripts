if [ -z "$1" ]
  then
    echo "Supply path (null if apex)"
    exit 1
fi
if [ -z "$2" ]
  then
    echo "Supply port"
    exit 1
fi
echo "location /$1/ {"
printf "        rewrite ^/$1(.*)"
echo ' $1 break;'
echo '        proxy_set_header X-Real-IP $remote_addr;'
echo '        proxy_set_header Host $host;'
echo '        proxy_set_header X-NginX-Proxy true;'
echo "        proxy_set_header Connection 'upgrade';"
echo '        proxy_set_header Upgrade $http_upgrade;'
echo "        proxy_pass http://127.0.0.1:$2;"
echo '        proxy_http_version 1.1;'
echo '        proxy_redirect off;'
echo '    }'
echo " "
echo " "
echo "---END LOCATION BLOCK---"
echo "---ROOT BLOCK---"
echo " "
echo " "
echo 'location / {'
echo "        proxy_pass http://127.0.0.1:$2;"
echo '        proxy_http_version 1.1;'
echo '        proxy_set_header X-Real-IP $remote_addr;'
echo '        proxy_set_header Upgrade $http_upgrade;'
echo "        proxy_set_header Connection 'upgrade';"
echo '        proxy_set_header Host $host;'
echo '        proxy_set_header X-NginX-Proxy true;'
echo '        proxy_cache_bypass $http_upgrade;'
echo '    }'
