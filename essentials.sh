#~/bin/bash
sudo apt-get update

#ESSENTIAL PACKAGES
sudo apt-get install -y git build-essential bash-completion aptitude unzip unrar-free p7zip logwatch unattended-upgrades fail2ban htop

#ESSENTIAL CONFIGS
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https
sudo ufw enable
echo "FIREWALL STATUS:"
sudo ufw status

sudo cp CONFIGS/10periodic /etc/apt/apt.conf.d/10periodic
sudo cp CONFIGS/50unattended-upgrades /etc/apt/apt.conf.d/50unattended-upgrades

sudo sed -i 's/--output mail/--output mail --mailto liam@bigelow.io --detail high/g' /etc/cron.daily/00logwatch
